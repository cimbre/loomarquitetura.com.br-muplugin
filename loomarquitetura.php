<?php
/*
Plugin Name: Loom Arquitetura
Plugin URI: https://loomarquitetura.com.br
Description: Customizações para o site Loom Arquitetura
Author: Cimbre
Version: 1.0.0.6
Author URI: https://cimbre.com.br
Text Domain: loomarquitetura
*/

/**
 * Load Translation
 */
function loomarquitetura_load_textdomain() 
{
    load_muplugin_textdomain('loomarquitetura', basename(dirname(__FILE__)) . '/languages');
}
add_action('plugins_loaded', 'loomarquitetura_load_textdomain');


/**
 * Register Projects Custom post type
 */

function cpt_loomarquitetura_projects() 
{
    $labels = array(
            'name'                  => __('Projects', 'loomarquitetura'),
            'singular_name'         => __('Project', 'loomarquitetura'),
            'menu_name'             => __('Projects', 'loomarquitetura'),
            'name_admin_bar'        => __('Project', 'loomarquitetura'),
            'archives'              => __('Project Archives', 'loomarquitetura'),
            'attributes'            => __('Project Attributes', 'loomarquitetura'),
            'parent_item_colon'     => __('Parent Project:', 'loomarquitetura'),
            'all_items'             => __('All Projects', 'loomarquitetura'),
            'add_new_item'          => __('Add New Project', 'loomarquitetura'),
            'add_new'               => __('Add New', 'loomarquitetura'),
            'new_item'              => __('New Project', 'loomarquitetura'),
            'edit_item'             => __('Edit Project', 'loomarquitetura'),
            'update_item'           => __('Update Project', 'loomarquitetura'),
            'view_item'             => __('View Project', 'loomarquitetura'),
            'view_items'            => __('View Projects', 'loomarquitetura'),
            'search_items'          => __('Search Project', 'loomarquitetura'),
            'not_found'             => __('Not found', 'loomarquitetura'),
            'not_found_in_trash'    => __('Not found in Trash', 'loomarquitetura'),
            'featured_image'        => __('Featured Image', 'loomarquitetura'),
            'set_featured_image'    => __('Set featured image', 'loomarquitetura'),
            'remove_featured_image' => __('Remove featured image', 'loomarquitetura'),
            'use_featured_image'    => __('Use as featured image', 'loomarquitetura'),
            'insert_into_item'      => __('Insert into project', 'loomarquitetura'),
            'uploaded_to_this_item' => __('Uploaded to this project', 'loomarquitetura'),
            'items_list'            => __('Projects list', 'loomarquitetura'),
            'items_list_navigation' => __('Projects list navigation', 'loomarquitetura'),
            'filter_items_list'     => __('Filter projects list', 'loomarquitetura'),
    );
    $rewrite = array(
            'slug'                  => __('projects', 'loomarquitetura'),
            'with_front'            => false,
            'pages'                 => true,
            'feeds'                 => true,
    );
    $args = array(
            'label'                 => __('Project', 'loomarquitetura'),
            'description'           => __('Loom Arquitetura Projects', 'loomarquitetura'),
            'labels'                => $labels,
            'supports'              => array('title', 'thumbnail', 'revisions', 'page-attributes'),
            'taxonomies'            => array('category', 'post_tag'),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'menu_icon'             => 'dashicons-admin-multisite',
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
    );
    register_post_type('loomarq_projects', $args); 
} 
add_action('init', 'cpt_loomarquitetura_projects', 0);

/**
 * Register Projects Custom fields
 */
function cmb_loomarquitetura_projects() 
{
    $section = 'project_';
    $cmb_projects = new_cmb2_box(
        array(
            'id'            => 'loomarquitetura_projects',
            'title'         => __('Project', 'loomarquitetura'),
            'object_types'  => array( 'loomarq_projects',), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
            // 'cmb_styles' => false, // false to disable the CMB stylesheet
            // 'closed'     => true, // Keep the metabox closed by default
        )
    );

    // Home description
    $cmb_projects->add_field( 
        array(
            'name'       => __('Home description', 'loomarquitetura'),
            'desc'       => '',
            'id'         => '_loomarquitetura_'.$section.'home_desc',
            'type'       => 'textarea_small',
            //'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
            // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
            // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
            // 'on_front'        => false, // Optionally designate a field to wp-admin only
            // 'repeatable'      => true,
        )
    );

    // Subtitle
    $cmb_projects->add_field( 
        array(
            'name'       => __('Subtitle', 'loomarquitetura'),
            'desc'       => '',
            'id'         => '_loomarquitetura_'.$section.'subtitle',
            'type'       => 'text',
            //'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
            // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
            // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
            // 'on_front'        => false, // Optionally designate a field to wp-admin only
            // 'repeatable'      => true,
        )
    );

    // Description
    $cmb_projects->add_field( 
        array(
            'name'       => __('Description', 'loomarquitetura'),
            'desc'       => '',
            'id'         => '_loomarquitetura_'.$section.'description',
            'type'       => 'textarea'
        ) 
    );

    // Highlights group
    $loomarquitetura_project_highlights_id = $cmb_projects->add_field(
        array(
            'id'          => '_loomarquitetura_'.$section.'highlights',
            'type'        => 'group',
            'description' => __('Projects highlights', 'loomarquitetura'),
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                    'group_title'   => __('Highlight {#}', 'loomarquitetura'), // since version 1.1.4, {#} gets replaced by row number
                    'add_button'    => __('Add Another Highlight', 'loomarquitetura'),
                    'remove_button' => __('Remove Highlight', 'loomarquitetura'),
                    'sortable'      => true, // beta
                    // 'closed'     => true, // true to have the groups closed by default
            ),
        ) 
    );

    // Highlights fields
    //Title
    $cmb_projects->add_group_field( 
        $loomarquitetura_project_highlights_id, array(
            'name' => 'Title',
            'id'   => 'title',
            'type' => 'text',
            // 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
        ) 
    );
    
    $cmb_projects->add_group_field( 
        $loomarquitetura_project_highlights_id, array(
            'name' => __('Description', 'loomarquitetura'),
            'description' => '',
            'id'   => 'description',
            'type' => 'textarea_small',
        )
    );
    
    $cmb_projects->add_group_field( 
        $loomarquitetura_project_highlights_id, array(
            'name' => __('Image', 'loomarquitetura'),
            'id'   => 'image',
            'type' => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'loomarquitetura'), // Change upload button text. Default: "Add or Upload File"
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                    ),
            ),
            'preview_size' => array(250, 141) //'large', // Image size to use when previewing in the admin.
        ) 
    );

    //Gallery Group Title
    $cmb_projects->add_field( 
        array(
        'name' => __('Gallery', 'loomarquitetura'),
        'desc' => '',
        'type' => 'title',
        'id'   => '_loomarquitetura_'.$section.'gallery_group_title',
        )
    );

    //Gallery  Title
    $cmb_projects->add_field( 
        array(
            'name' => __('Gallery title', 'loomarquitetura'),
            'desc' => '',
            'type' => 'text',
            'id'   => '_loomarquitetura_'.$section.'gallery_title',
        )
    );

    //Gallery Description
    $cmb_projects->add_field(
        array(
            'name' => __('Gallery description', 'loomarquitetura'),
            'description' => '',
            'id'   =>  '_loomarquitetura_'.$section.'gallery_description',
            'type' => 'textarea_small',
        )
    );

    //Gallery Images
    $cmb_projects->add_field(
        array(
            'name' => __('Gallery images', 'loomarquitetura'),
            'id'   =>  '_loomarquitetura_'.$section.'gallery_images',
            'type' => 'file_list',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text' => array(
                'add_upload_files_text' => __('Add Images', 'loomarquitetura'), // default: "Add or Upload Files"
                'remove_image_text' => __('Remove Image', 'loomarquitetura'), // default: "Remove Image"
                'file_text' => __('File', 'loomarquitetura'), // default: "File:"
                'file_download_text' => __('Download', 'loomarquitetura'), // default: "Download"
                'remove_text' => __('Remove', 'loomarquitetura'), // default: "Remove"
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                    ),
            ),
            'preview_size' => array(250, 141) //'large', // Image size to use when previewing in the admin.
        )
    );
}
add_action('cmb2_admin_init', 'cmb_loomarquitetura_projects');

/**
 * Register Services Custom fields
 */
function cmb_loomarquitetura_services() 
{
    $section = 'service_';
    $cmb_services = new_cmb2_box(
        array(
            'id'            => 'loomarquitetura_services',
            'title'         => __('Services', 'loomarquitetura'),
            'object_types'  => array('page'), // post type
            'show_on_cb' => 'loomarquitetura_show_on_page_services', // function should return a bool value
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
            // 'cmb_styles' => false, // false to disable the CMB stylesheet
            // 'closed'     => true, // Keep the metabox closed by default
        )
    );

    //Featured Title
    $cmb_services->add_field( 
        array(
            'name' => __('Featured title', 'loomarquitetura'),
            'desc' => '',
            'type' => 'text',
            'id'   => '_loomarquitetura_'.'featured_title',
        )
    );

    //Services group
    $loomarquitetura_services_id = $cmb_services->add_field( 
        array(
            'id'          => '_loomarquitetura_' . 'services',
            'type'        => 'group',
            'description' => __('Services', 'loomarquitetura'),
            // 'repeatable'  => false, // use false if you want non-repeatable group
            'options'     => array(
                'group_title'   => __('Service {#}', 'loomarquitetura'), // since version 1.1.4, {#} gets replaced by row number
                'add_button'    => __('Add Another Service', 'loomarquitetura'),
                'remove_button' => __('Remove Service', 'loomarquitetura'),
                'sortable'      => true, // beta
                // 'closed'     => true, // true to have the groups closed by default
            ),
        )
    );
    
    // Icon
    $cmb_services->add_group_field(
        $loomarquitetura_services_id, array(
            'name'    => __('Icon', 'loomarquitetura'),
            'desc'    => "",
            'id'      => 'icon',
            'type'    => 'file',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text'    => array(
                    'add_upload_file_text' => __('Add icon', 'loomarquitetura'), // Change upload button text. Default: "Add or Upload File"
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                            'image/svg+xml',
                    ),
            ),
            'preview_size' => array(150, 150) //'large', // Image size to use when previewing in the admin.
        ) 
    );

    // Service
    $cmb_services->add_group_field(
        $loomarquitetura_services_id, array(
            'name'       => __('Service', 'loomarquitetura'),
            'desc'       => '',
            'id'         => 'service',
            'type'       => 'text',
            //'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
            // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
            // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
            // 'on_front'        => false, // Optionally designate a field to wp-admin only
            // 'repeatable'      => true,
        )
    );

    // Description
    $cmb_services->add_group_field(
        $loomarquitetura_services_id, array(
            'name'       => __('Description', 'loomarquitetura'),
            'desc'       => '',
            'id'         => 'description',
            'type'       => 'textarea'
        ) 
    );
}

function loomarquitetura_show_on_page_services($cmb) 
{
    $slug = get_page_template_slug($cmb->object_id);
    return in_array($slug, array('views/template-servicos.blade.php'));
}
add_action('cmb2_admin_init', 'cmb_loomarquitetura_services');

/**
 * Register About Custom fields
 */
function cmb_loomarquitetura_about() 
{
    $section = 'about_';
    $cmb_about = new_cmb2_box(
        array(
            'id'            => 'loomarquitetura_about',
            'title'         => __('About', 'loomarquitetura'),
            'object_types'  => array('page'), // post type
            'show_on_cb' => 'loomarquitetura_show_on_page_about', // function should return a bool value
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
            // 'cmb_styles' => false, // false to disable the CMB stylesheet
            // 'closed'     => true, // Keep the metabox closed by default
        )
    );

    //Featured Title
    $cmb_about->add_field( 
        array(
            'name' => __('Featured title', 'loomarquitetura'),
            'desc' => '',
            'type' => 'text',
            'id'   => '_loomarquitetura_'.'featured_title',
        )
    );

    //Gallery Group Title
    $cmb_about->add_field( 
        array(
        'name' => __('Gallery', 'loomarquitetura'),
        'desc' => '',
        'type' => 'title',
        'id'   => '_loomarquitetura_'.$section.'gallery_group_title',
        )
    );

    //Gallery  Title
    $cmb_about->add_field( 
        array(
            'name' => __('Gallery title', 'loomarquitetura'),
            'desc' => '',
            'type' => 'text',
            'id'   => '_loomarquitetura_'.$section.'gallery_title',
        )
    );

    //Gallery Description
    $cmb_about->add_field(
        array(
            'name' => __('Gallery description', 'loomarquitetura'),
            'description' => '',
            'id'   =>  '_loomarquitetura_'.$section.'gallery_description',
            'type' => 'textarea_small',
        )
    );

    //Gallery Images
    $cmb_about->add_field(
        array(
            'name' => __('Gallery images', 'loomarquitetura'),
            'id'   =>  '_loomarquitetura_'.$section.'gallery_images',
            'type' => 'file_list',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text' => array(
                'add_upload_files_text' => __('Add Images', 'loomarquitetura'), // default: "Add or Upload Files"
                'remove_image_text' => __('Remove Image', 'loomarquitetura'), // default: "Remove Image"
                'file_text' => __('File', 'loomarquitetura'), // default: "File:"
                'file_download_text' => __('Download', 'loomarquitetura'), // default: "Download"
                'remove_text' => __('Remove', 'loomarquitetura'), // default: "Remove"
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                    ),
            ),
            'preview_size' => array(250, 141) //'large', // Image size to use when previewing in the admin.
        )
    );
}

function loomarquitetura_show_on_page_about($cmb) 
{
    $slug = get_page_template_slug($cmb->object_id);
    return in_array($slug, array('views/template-empresa.blade.php'));
}
add_action('cmb2_admin_init', 'cmb_loomarquitetura_about');

/**
 * Register Contact Custom fields
 */
function cmb_loomarquitetura_contact() 
{
    $section = 'contact_';
    $cmb_contact = new_cmb2_box(
        array(
            'id'            => 'loomarquitetura_contact',
            'title'         => __('contact', 'loomarquitetura'),
            'object_types'  => array('page'), // post type
            'show_on_cb' => 'loomarquitetura_show_on_page_contact', // function should return a bool value
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
            // 'cmb_styles' => false, // false to disable the CMB stylesheet
            // 'closed'     => true, // Keep the metabox closed by default
        )
    );

    //Featured Title
    $cmb_contact->add_field( 
        array(
            'name' => __('Featured title', 'loomarquitetura'),
            'desc' => '',
            'type' => 'text',
            'id'   => '_loomarquitetura_'.'featured_title',
        )
    );

    //Contact Address
    $cmb_contact->add_field( 
        array(
            'name' => __('Address', 'loomarquitetura'),
            'id'   => '_loomarquitetura_'.$section.'address',
            'type' => 'text',
        ) 
    );

    //Contact Phone
    $cmb_contact->add_field( 
        array(
            'name' => __('Phone', 'loomarquitetura'),
            'id'   => '_loomarquitetura_'.$section.'phone',
            'type' => 'text',
        ) 
    );

    //Contact Phone
    $cmb_contact->add_field( 
        array(
            'name' => __('Cell Phone', 'loomarquitetura'),
            'id'   => '_loomarquitetura_'.$section.'cell-phone',
            'type' => 'text',
        ) 
    );
}

function loomarquitetura_show_on_page_contact($cmb) 
{
    $slug = get_page_template_slug($cmb->object_id);
    return in_array($slug, array('views/template-contato.blade.php'));
}
add_action('cmb2_admin_init', 'cmb_loomarquitetura_contact');

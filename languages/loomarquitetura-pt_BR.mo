��    ;      �  O   �           	          %  	   9  
   C     N     V     f     o     w  
   �     �     �     �     �     �     �     �     �     �               "     0     A     F     L     `     z  	   �     �     �     �     �     �     �     �     �               )     0     A     N     ]     s     �     �     �     �     �     �     �     �     �                 �  %     �	     �	     �	     �	     
     "
     1
     H
  	   Y
     c
     u
     �
     �
     �
     �
     �
     �
     �
     �
     �
          "     5     B     V     ]     d     w     �     �     �     �     �     �     �     �               .      @     a     i     z     �     �     �     �     �  	   �     �  
   �               6     R     ^     k     s     -   "   %      (   3           9       7          0          	                                       1          2   +       ;   5      .         !            /                           '             
   &   ,      :              $      )      8   4   6             #             *    About Add Another Highlight Add Another Service Add Image Add Images Add New Add New Project Add icon Address All Projects Cell Phone Description Download Edit Project Featured Image Featured title File Filter projects list Gallery Gallery description Gallery images Gallery title Highlight {#} Home description Icon Image Insert into project Loom Arquitetura Projects New Project Not found Not found in Trash Parent Project: Phone Project Project Archives Project Attributes Projects Projects highlights Projects list Projects list navigation Remove Remove Highlight Remove Image Remove Service Remove featured image Search Project Service Service {#} Services Set featured image Subtitle Update Project Uploaded to this project Use as featured image View Project View Projects contact projects Project-Id-Version: loom-projects
POT-Creation-Date: 2018-08-09 18:38-0300
PO-Revision-Date: 2018-08-09 18:39-0300
Last-Translator: 
Language-Team: 
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: __;_x
X-Poedit-SearchPath-0: loomarquitetura.php
 Sobre Adicionar Novo Destaque Adicionar Novo Serviço Adicionar imagem Adicionar imagens Adicionar Novo Adicionar Novo Projeto Adicionar ícone Endereço Todos os Projetos Telefone Celular Descrição Download Editar Projeto Imagem em Destaque Título destaque Arquivo Filtrar lista de projetos Galeria Descrição da Galeria Imagens da Galeria Título da Galeria Destaque {#} Descrição na Home Ícone Imagem Inserir no projeto Projetos Loom Arquitetura Novo Projeto Não encontrado Não encontrado na Lixeira Projeto pai: Telefone Projeto Arquivo de Projetos Atributos do Projeto Projetos Destaques do projeto Lista de projetos Navegação na lista de projetos Remover Remover Destaque Remover imagem Remover Serviço Remover imagem destacada Procurar Projeto Serviço Serviço {#} Serviços Definir imagem destacada Subtítulo Atualizar Projeto Carregado para este projeto Use como imagem em destaque Ver Projeto Ver Projetos Contato projetos 